# -*- coding: utf-8 -*- 

slide = (
        ('000_SEoperations.png', 'Sync Engine basic operations'),
        ('000_SyncProcesses.png', 'Sync Processes list'),
        ('096_Difference_provision_status_values.png', 'Difference provision status values'),
        ('098_ProvisionStatus_state_transitions.png', 'ProvisionStatus state transitions'),
        ('099_ProvisionStatus_colours.png', 'ProvisionStatus colours'),
        ('100_Adapter.png', 'Adapter executable'),
        ('101_ManagemenDomain.png', 'Management Domains'),
        ('102_ManagemenDomainCramer.png', 'Management Domains in Cramer'),
        ('103_ManagemenDomainSQL.png', 'Management Domains list'),
        ('104_MarconiDomainCramer.png', 'Marconi Management Domain in Cramer'),
        ('105_MarconiDomainSQL.png', 'Marconi Management Domain'),
        ('201_UDMGenerator.png', 'UDMGenerator'),
        ('202_UPKCache.png', 'UPKCache'),
        ('203_UDM.png', 'UDM document containing the Cramer Inventory object'),
        ('204_Matching.png', 'Matching'),
        ('206_SEComparison.png', 'SE Comparison'),
        ('207_SEComparison_UDMElement.png', 'SE Comparison: UDMElement & UDMAttribute'),
        ('208_SEComparison.png', 'SE Comparison: UDMElement & UDMAttribute tables'),
        ('209_SEComparison_Attributes.png', 'SE Comparison: Attributes'),
        ('210_SEComparison_Blank.png', 'SE Comparison: Blank elements'),
        ('211_SEComparison_Identifier.png', 'SE Comparison: an element identifier'),
        ('212_SEComparison_PrimaryAndSecondary.png', 'SE Comparison: Primary and Secondary elements'),
        ('213_SEComparison_ElementComparisonRule.png', 'SE Comparison: ElementComparisonRule table'),
        ('214_SEComparison_AttributeComparisonRule.png', 'SE Comparison: AttributeComparisonRule table'),
        ('214_SEComparison_Comparison_Engine_algorithm.png', 'Comparison Engine algorithm'),
        ('215_SEComparison_adapter.png', 'SE Comparison: Adapter & Adapter filters'),
        ('220_example_step1.png', 'Example A: Get SyncProcess list for Tellabs SyncProfiles'),
        ('220_example_step2.png', 'Example A: Tellabs SyncProfiles'),
        ('220_example_step3.png', 'Example A: Tellabs SyncRules'),
        ('220_example_step4.png', 'Example A: Tellabs SyncDiff filters'),
        ('220_example_step5.png', 'Example A: RequestInstance'),
        ('220_example_step5_note.png', 'Example A Note: DoNotProcess flag set to 1'),
        ('220_example_step5_note2.png', 'Example A Note: RequestInstance & SyncDiff'),
        ('220_example_step5_note3.png', 'Example A Note: Preceding differences'),
        
        ('220_example_step61.png', 'Example A: Difference Processing 1'),
        ('220_example_step62.png', 'Example A: Difference Processing 2'),
        ('300_Cramer.png', 'Example B: Circuit not in external system'),
        ('300_SyncSchedule.png', 'Example B: SyncSchedule'),
        ('301_SyncProcesses.png', 'Example B: SyncProcesses'),
        ('302_SyncProfile.png', 'Example B: SyncProfile'),
        ('303_SyncRules.png', 'Example B: SyncRules'),
        ('304_DifferenceProcessing1.png', 'Example B: Difference Processing 1'),
        ('305_DifferenceProcessing2.png', 'Example B: Difference Processing 2'),
        ('306_removeTellabsCircuit.png', 'Example B: removeTellabsCircuit(...)'),
        ('307_RequestInstance.png', 'Example B: RequestInstance'),
        ('308_SyncDiffs.png', 'Example B: SyncDiffs'),
        )


tmpl_index = """
<html>
<head>
   <title>Cramer SyncEngine</title>  
   <link rel="stylesheet" href="style.css">
</head>
<body>
    <table class="sample" align="center" width="900">
    <th>Cramer SyncEngine Workshop</th>
    <tr>
        <td>
            <ol class="d">
{0}
            </ol>
        </td>
    </tr>
    <tr><td>Ver.0.2 04 Dec, 2014. &lt;Vadym.Shpuryk&#x40snt.ua&gt;</td></tr>
    </table>
</body>
</html>
"""
tmpl_li = '<li><a href="{0}">{1}</a></li>'

tmpl_page = """
<html>
<head>
   <title>Cramer SyncEngine</title>  
   <link rel="stylesheet" href="style.css">
</head>
<body>
    <table class="sample">
    <tr><td><img src="images/{0}" alt="Smiley face" height="700" width="900"></td></tr>
    <tr align="center"><td>{1}</td></tr>
    </table>
</body>
</html>
"""


tmpl_back = '&lt;<a href="{0}">Back</a>'
tmpl_nav = '{0} <a href="index.html">Content</a> {1}'
tmpl_next = '<a href="{0}">Next</a>&gt;'


def main():
    oldata = list()
    numb = len(slide)
    for i,x in enumerate(slide):
        pagename = x[0].split('.')[0] + '.html'
        oldata.append(tmpl_li.format(pagename, x[1]))

        if i == 0:
            prevlink = '&lt;Back'
            nextlink = tmpl_next.format(slide[i+1][0].split('.')[0] + '.html')
        elif i == numb-1:
            prevlink = tmpl_back.format(slide[i-1][0].split('.')[0] + '.html')
            nextlink = 'Next&gt;'
        else:
            prevlink = tmpl_back.format(slide[i-1][0].split('.')[0] + '.html')
            nextlink = tmpl_next.format(slide[i+1][0].split('.')[0] + '.html')

        nav = tmpl_nav.format(prevlink, nextlink)

        page_content = tmpl_page.format(x[0], nav)
        try:
            # This will create a new file or **overwrite an existing file**.
            f = open(pagename, "w")
            try:
                f.write(page_content)
            finally:
                f.close()
        except IOError:
            pass


    index_content = tmpl_index.format('\n'.join(oldata))
    try:
        # This will create a new file or **overwrite an existing file**.
        f = open('index.html', "w")
        try:
            f.write(index_content)
        finally:
            f.close()
    except IOError:
        pass


if __name__ == '__main__':
    main()
